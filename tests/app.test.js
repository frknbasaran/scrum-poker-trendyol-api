const assert = require('assert');
const chai = require('chai');
const chaiHttp = require('chai-http');
const { spawn } = require('child_process');

const serviceURL = 'http://localhost';

chai.use(chaiHttp);

let project;

describe('API TEST', () => {

    describe('Start web service', function(){
        this.timeout(60000);
        it('should start web service', (done) => {
            project = spawn('npm', ['start']);
            setTimeout(done, 5000);
        });
    });

    describe('POST /auth/register', () => {
       it('should return user object', (done) => {
         chai.request(serviceURL).post('/auth/register')
             .type('form')
             .send({
                 'username': 'tester_user_123',
                 'password': '123'
             })
             .end((err, res) => {
                 assert.equal(res.status, 200);
                 assert.equal(res.body.username, 'tester_user_123');
                 assert.equal(res.body.auth_level, 1);
                 done();
             })
       });
    });

    describe('POST /auth/login', () => {
        it('should return wrong credentials error', (done) => {
            chai.request(serviceURL).post('/auth/login')
                .type('form')
                .send({
                    'username': 'tester_user_132',
                    'password': '123'
                })
                .end((err, res) => {
                    assert.equal(res.status, 401);
                    done();
                })
        });
    });

    describe('POST /auth/login', () => {
        it('should return authentication object', (done) => {
            chai.request(serviceURL).post('/auth/login')
                .type('form')
                .send({
                    'username': 'tester_user_123',
                    'password': '123'
                })
                .end((err, res) => {
                    assert.equal(res.status, 200);
                    assert.equal(res.body.user.username, 'tester_user_123');
                    assert.equal(res.body.user.auth_level, 1);
                    done();
                })
        });
    });

    describe('POST /sessions', () => {
       it('should return authentication error without token', (done) => {
           chai.request(serviceURL).post('/sessions')
               .type('form')
               .send({
                   'name': 'Scrum Session Name',
                   'stories': [{'name':'story name', 'votes':[]}]
               })
               .end((err, res) => {
                   assert.equal(res.status, 401);
                   done();
               });
       })
    });

    describe('POST /sessions/:id/vote', () => {
        it('should return authentication error without token', (done) => {
            chai.request(serviceURL).post('/sessions/5be86c6c9bfc372c6240eaab/vote')
                .type('form')
                .send({
                    'story': '5be86c6c9bfc372c6240eaab',
                    'vote':{'voter':'5be86c6c9bfc372c6240eaab','vote':14}
                })
                .end((err, res) => {
                    assert.equal(res.status, 401);
                    done();
                });
        })
    });

    describe('POST /sessions/:id/end-story', () => {
        it('should return authentication error without token', (done) => {
            chai.request(serviceURL).post('/sessions/5be86c6c9bfc372c6240eaab/end-story')
                .type('form')
                .send({
                    'finalScore': 'Scrum Session Name',
                    'story':'5be86c6c9bfc372c6240eaab'
                })
                .end((err, res) => {
                    assert.equal(res.status, 401);
                    done();
                });
        })
    });

    describe('GET /sessions/:id', () => {
        it('should return authentication error without token', (done) => {
            chai.request(serviceURL).get('/sessions/5be86c6c9bfc372c6240eaab')
                .end((err, res) => {
                    assert.equal(res.status, 401);
                    done();
                });
        })
    });

});

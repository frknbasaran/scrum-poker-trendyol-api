import mongoose from 'mongoose';
import UserSchema from "../models/user";
import Database from './connection';
const User = Database.model('User', UserSchema);

export default {
    validateUsername: (username) => {
        return /^[a-zA-Z0-9]+$/.test(username);
    },
    isThisUsernameAvailable: async (username) => {
        let userExistWithThisUsername = await User.findOne({
            "username": username
        });
        if (userExistWithThisUsername) return false;
        else return true;
    },
    validateObjectId: async (objectId) => {
        return mongoose.Types.ObjectId.isValid(objectId);
    },
    random: () => {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

        for (var i = 0; i < 5; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

        return text;
    }
};

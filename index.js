import Koa from 'koa';
import BodyParser from 'koa-bodyparser';
import DotEnv from 'dotenv';
import KoaQS from 'koa-qs';
import KCors from 'koa2-cors';
// initalize socket.io
const io = require('socket.io')(1111);

// routes
import UserRouter from './routes/user';
import BrandRouter from './routes/session';

export default (async () => {
    // Create app as a Koa Instance
    const app = new Koa();

    // Activate qs module
    KoaQS(app);
    // initalize dotenv
    DotEnv.config();

    /* Handle socket requests */
    io.on('connection', (socket) => {
        // handle new story request
        socket.on('increaseActiveStoryIndex', () => {
            socket.broadcast.emit('increaseActiveStoryIndex');
        });
        // handle vote request
        socket.on('vote', () => {
            socket.broadcast.emit('updateVotes');
        });
    })

    app
        // koa cors configuration
        .use(KCors({origin: '*'}))
        // inital body parser
        .use(BodyParser()).use(async (ctx, next) => {
            try {
                await next();
                if (ctx.body == null) {
                    ctx.throw(404, 'NOT_FOUND');
                }
            } catch (e) {
                ctx.status = e.status || 500;
                ctx.body = e.message;
                if (ctx.status == 500) console.error(e);
            }
        });
    // call routes
    app.use(UserRouter.routes())
        .use(BrandRouter.routes())

    // run server
    await new Promise(resolve => app.listen(process.env.PORT, resolve));
    app.url = 'http://localhost:' + process.env.PORT;
    return app;
})();

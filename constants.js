export default {
	API_messages: {
		Common: {
			Main: 'READ_DOC',
			NotFound: 'NOT_FOUND',
			Unauthorized: 'UNAUTHORIZED',
			InvalidObjectId: 'INVALID_OBJECT_ID',
			SomethingWentWrong: 'SOMETHING_WENT_WRONG'
		},
		User: {
			InvalidEmail: 'EMAIL_INVALID_OR_IS_EXIST_OR_NOT_PROVIDED',
			InvalidOrInefficientPassword: 'PASSWORD_NOT_MEET_REQUIREMENTS_OR_NOT_PROVIDED',
			ProvidePassword: 'PASSWORD_NOT_PROVIDED',
			InvalidCredentials: 'WRONG_CREDENTIALS',
			InvalidUsername: 'INVALID_USERNAME_OR_NOT_PROVIDED',
			PasswordFieldsNotProvided: 'PASSWORD_FIELDS_NOT_PROVIDED',
			UsernameAlreadyExist: 'USERNAME_ALREADY_EXIST'
		},
		Session: {
			IDFieldMustBeProvided: 'ID_FIELD_MUST_BE_PROVIDED'
		}
	}
};

// external dependencies
import md5 from 'md5';
import {
    uid
} from 'rand-token';
// internal dependencies
import UserSchema from '../models/user';
import TokenSchema from '../models/token';
import Database from '../utils/connection';
import Constants from '../constants';

// pre-definations
const User = Database.model('User', UserSchema);
const Token = Database.model('Token', TokenSchema);

export default {
    logIn: async (ctx) => {
        if (ctx.request.body.username) var username = ctx.request.body.username;
        else ctx.throw(401, Constants.API_messages.User.InvalidUsername);
        // validate password
        if (ctx.request.body.password) var password = ctx.request.body.password;
        else ctx.throw(401, Constants.API_messages.User.ProvidePassword);
        let ipAddr = ctx.ip;
        // exec query
        const user = await User.findOne({
            $and: [{
                "username": username
            }, {
                "password": md5(password)
            }]
        });
        /*
        * Auth Levels:
        * 1: developer
        * 2: scrum master
        */
        if (user) {
            // is exist token for this user?
            const existedToken = await Token.findOne({
                "user": user._id
            });
            // is there non-expired token?
            if (existedToken && (existedToken.expired_at > Date.now())) {
                ctx.body = {user, token: existedToken};
                return;
            }
            // there is no token or expired
            else {
                try {
                    // remove expired token if exist
                    await Token.deleteMany({"user": user._id});
                    // create new token instance
                    const generatedToken = new Token({
                        ip: ipAddr,
                        token: uid(32),
                        user: user._id,
                        // expire in 1 week
                        expired_at: Date.now() + 604800000
                    });
                    // define new token
                    let token = await generatedToken.save();
                    ctx.body = {user, token};
                } catch (mongoError) {
                    ctx.throw(200, mongoError.message);
                }
            }
        } else ctx.throw(401, Constants.API_messages.User.InvalidCredentials);
    },
    register: async (ctx) => {
        if (ctx.request.body.username) var username = ctx.request.body.username;
        else {
            ctx.throw(401, Constants.API_messages.User.InvalidUsername);
            return;
        }
        // validate password
        if (ctx.request.body.password) var password = ctx.request.body.password;
        else {
            ctx.throw(401, Constants.API_messages.User.ProvidePassword);
            return;
        }
        // try create new user
        try {
            let isUsernameExist = await User.findOne({username: username});
            if (isUsernameExist) {
                ctx.throw(401, Constants.API_messages.User.UsernameAlreadyExist);
                return;
            }
            let user = new User({
                username,
                password: md5(password)
            });
            ctx.body = await user.save();
        } catch (mongoError) {
            ctx.throw(500, mongoError.message);
        }
    },
    isScrumMaster: async (ctx, next) => {
        try {
            let matchedToken = await Token.findOne({
                "token": ctx.request.headers.token
            }).populate('user', 'auth_level');
            if (matchedToken) {
                /*
                *  update "last_updated" and prepend "expired_at"
                *  properties when user realized activity
                */
                matchedToken.last_updated = Date.now();
                matchedToken.expired_at = Date.now() + 604800000;
                await matchedToken.save();
                if (matchedToken.user.auth_level == 2) await next();
                else ctx.throw(401, Constants.API_messages.Common.Unauthorized);
            } else ctx.throw(401, Constants.API_messages.Common.Unauthorized);
        } catch (error) {
            ctx.throw(401, error.message);
        }
    },
    isLoggedIn: async (ctx, next) => {
        try {
            let matchedToken = await Token.findOne({
                "token": ctx.request.headers.token
            }).populate('user', 'auth_level');
            if (matchedToken) {
                /*
                *  update "last_updated" and prepend "expired_at"
                *  properties when user realized activity
                */
                matchedToken.last_updated = Date.now();
                matchedToken.expired_at = Date.now() + 604800000;
                await matchedToken.save();
                if (matchedToken.user.auth_level >= 1) await next();
                else {
                    ctx.throw(401, Constants.API_messages.Common.Unauthorized);
                }
            } else ctx.throw(401, Constants.API_messages.Common.Unauthorized);
        } catch (error) {
            ctx.throw(401, error.message);
        }
    },
    getUserFromToken: async (ctx) => {
        let token = await Token.findOne({
            "token": ctx.request.headers.token
        });
        return token.user;
    }
}

import SessionSchema from '../models/session';
import Database from '../utils/connection';
import Constants from '../constants';
import Auth from './auth';

const Session = Database.model('Session', SessionSchema);

export default {
    create: async (ctx) => {
        try {
            const session = new Session({
                name: ctx.request.body.name,
                created_by: await Auth.getUserFromToken(ctx),
                stories: ctx.request.body.stories,
                numberOfVoters: ctx.request.body.numberOfVoters
            });
            ctx.body = await session.save();
        } catch (e) {
            ctx.throw(500, e.message);
        }
    },
    getOne: async (ctx) => {
        try {
            let session = await Session.findOne({
                _id: ctx.params.id
            }).select('name stories created_by created_at numberOfVoters activeStoryIndex').deepPopulate('stories.votes.voter');
            if (session) ctx.body = session;
            else ctx.throw(404, Constants.API_messages.Common.NotFound);
        } catch (e) {
            ctx.throw(500, e.message);
        }
    },
    vote: async (ctx) => {
        try {
            const session = await Session.findOne({
                _id: ctx.params.id
            });
            let story = ctx.request.body.story;
            let vote = ctx.request.body.vote;
            let voter = await Auth.getUserFromToken(ctx);
            let updatedOldVote = false;
            /*
            * Add vote into votes array of related story
            * */
            session.stories.forEach(s => {
               if (s._id+"" === story) {
                   // check this user has already vote in this story?
                   s.votes.forEach(v => {
                       if (v.voter+"" === voter+"") {
                           s.votes[s.votes.indexOf(v)] = {vote, voter};
                           updatedOldVote = true;
                       }
                   });
                   if (!updatedOldVote) s.votes.push({vote, voter});
               }
            });

            await session.save();
            ctx.body = await Session.findOne({
                _id: ctx.params.id
            }).select('name stories created_by created_at numberOfVoters activeStoryIndex').deepPopulate('stories.votes.voter');
        } catch (e) {
            ctx.throw(500, e.message);
        }
    },
    endStory: async (ctx) => {
        try {
            const session = await Session.findOne({
                _id: ctx.params.id
            });
            let story = ctx.request.body.story;
            let finalScore = ctx.request.body.finalScore;

            /*
            * Set final score
            * */
            session.stories.forEach(s => {
                if (s._id+"" === story) {
                    s.finalScore = finalScore;
                    s.status = 'Voted';
                }
            });
            session.activeStoryIndex++;
            await session.save();
            ctx.body = await Session.findOne({
                _id: ctx.params.id
            }).select('name stories created_by created_at numberOfVoters activeStoryIndex').deepPopulate('stories.votes.voter');
        } catch (e) {
            ctx.throw(500, e.message);
        }
    }
}

import mongoose from 'mongoose';

mongoose.Promise = Promise;

const Token = new mongoose.Schema({
    token: {
        type: String,
        required: true
    },
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    ip: {
        type: String
    },
    created_at: {
        type: Date,
        default: Date.now
    },
    expired_at: {
        type: Date
    },
    last_updated: {
        type: Date,
        default: Date.now
    }
});

export default Token;
import mongoose from 'mongoose';
import deepPopulate from 'mongoose-deep-populate';

mongoose.Promise = Promise;

const DeepPopulate = deepPopulate(mongoose);

const Session = new mongoose.Schema({
    name: {
        type: String
    },
    created_by: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    numberOfVoters: Number,
    created_at: {
        type: Date,
        default: Date.now
    },
    stories: [
        {
            name: String,
            votes: [
                {
                    vote: Number,
                    voter: {
                        type: mongoose.Schema.Types.ObjectId,
                        ref: 'User'
                    }
                }],
            finalScore: Number,
            status: {
                type: String,
                default: 'Not voted'
            }
        }],
    activeStoryIndex: {
        type: Number,
        default: 0
    }
});

Session.plugin(DeepPopulate, {
    populate: {
        'stories.votes.voter': {
            select: 'username'
        }
    }
});

export default Session;

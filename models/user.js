import mongoose from 'mongoose';
mongoose.Promise = Promise;

const User = new mongoose.Schema({
    username: {
        type: String,
        unique: true
    },
    auth_level: {
        type: Number,
        default: 1
    },
    password: {
        type: String
    },
    registered_at: {
        type: Date,
        default: Date.now
    }
},{ usePushEach: true });

export default User;

import { Auth, Session } from '../core';
import KoaRouter from 'koa-router';

const Router = new KoaRouter();

/*
* Create new session
* @POST
* @param: name {string} - Scrum session name
* @param: numberOfVoters {number} - Minimum voter count
* @param: stories {object} - Stories array
* @return Session {object} - returns created session object
* */
Router.post('/sessions', Auth.isScrumMaster, Session.create);

/*
* Get session by id
* @GET
* @param: :id {string} - Session id
* @return Session {object} - returns session object by id
* */
Router.get('/sessions/:id', Auth.isLoggedIn, Session.getOne);

/*
* Vote for story
* @POST
* @param: story {string} - Story id
* @param: vote {object} - Vote object that contains vote and voter info
* @return Session {object} - returns updated session object
* */
Router.post('/sessions/:id/vote', Auth.isLoggedIn, Session.vote);

/*
* End story
* @POST
* @param: story {string} - Story id
* @param: finalScore {number} - Final score for discussed story
* @return Session {object} - returns updated session object
* */
Router.post('/sessions/:id/end-story', Auth.isScrumMaster, Session.endStory);

export default Router;

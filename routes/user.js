import { Auth } from '../core';
import KoaRouter from 'koa-router';

const Router = new KoaRouter();

/*
* Login
* @POST
* @param username {string} - username
* @param password {string} - password
* @returns Authentication {object} - returns authentication object that contains user and token
* */
Router.post('/auth/login', Auth.logIn);

/*
* Register
* @POST
* @param username {string} - username
* @param password {string} - password
* @returns User {object} - returns created user object
* */
Router.post('/auth/register', Auth.register);

export default Router;

### Load dependencies
`npm install`

### Run tests
`npm test`

### Start service
`npm start`